import {Component, OnInit} from '@angular/core';
import {CarouselService} from './services/carousel.service';
import {Carousel} from './models/carousel/Caousel.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  carouseles: Carousel[];

  responsiveOptions;

  constructor(private carouselService: CarouselService) {
    this.responsiveOptions = [
      {
        breakpoint: '1024px',
        numVisible: 3,
        numScroll: 3
      },
      {
        breakpoint: '768px',
        numVisible: 2,
        numScroll: 2
      },
      {
        breakpoint: '560px',
        numVisible: 1,
        numScroll: 1
      }
    ];
  }

  ngOnInit() {
    this.carouseles = this.carouselService.getCarousels();
  }
}
