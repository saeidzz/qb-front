export class Carousel {
  imageName: string;
  title: string;
  description: string;


  constructor(imageName: string, title: string, description: string) {
    this.imageName = imageName;
    this.title = title;
    this.description = description;
  }
}
