import {Injectable} from '@angular/core';
import {Carousel} from '../models/carousel/Caousel.model';

@Injectable({
  providedIn: 'root'
})
export class CarouselService {

  constructor() {

  }
    getCarousels() {
      const cars = [
        new Carousel('1', 'image1', 'this is a test'),
        new Carousel('2', 'image1', 'this is a test'),
        new Carousel('3', 'image1', 'this is a test')
      ]

      return cars;
    }
}
